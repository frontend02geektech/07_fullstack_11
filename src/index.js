import './style.css'

function q(sel) {
    return document.querySelector(sel)
}

function getData(url) {
    return fetch(url).then(r => r.json())
}

// asdf

//fdas

async function main() {
    let users = await getData('/users');
    console.log('users', users)
    printUsers(users)

    let heroes = await getData('/heroes');
    console.log('heroes', heroes)

    for (let [index, hero] of heroes.entries()) {
        let div = document.createElement('div')
        div.innerHTML = `<b>${hero.name}</b>`;

        q('#heroes').append(div)

        div.addEventListener('click', async () => {
            let heroNew = await getData(`/hero/${index}`)
            Object.assign(hero, heroNew)
            div.innerHTML = `<b>${hero.name} [${hero.health}]</b>`;
        })
    }
}

let lastId = -1;

async function getMessages() {
    let msgs = await fetch('/messages').then(r => r.json());
    printMessages(msgs)
}

setInterval(getMessages, 1000)

function printUsers(users) {
    q('#users').innerHTML = '';

    for (let user of users) {
        let el = document.createElement('li');
        ((user) => {
            el.innerHTML = user.name;
            q('#users').append(el)

            el.addEventListener('click', () => {
                handleClickUser(user)
            })
        })(user)
    }
}

function handleClickUser(user) {
    console.log('user click', user)
    let name = user.name;

    q('#msg').value = `${name}, `;
    q('#msg').focus();
}

function printMessages(msgs) {

    let latestMsgs = msgs.filter(msg => msg.id > lastId)

    for (let {name, msg} of latestMsgs) {
        let el = document.createElement('li');
        el.innerHTML = `<b>${name}:</b> ${msg}`;
        q('#msgs').append(el)
    }

    if (msgs.length > 0)
        lastId = msgs[msgs.length - 1].id;
}

main();

let name
q('#login-form').addEventListener('submit', async e => {
    e.preventDefault()
    name = q('#username').value;
    console.log('name', name)

    let users = await fetch('/login', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name})
    }).then(r => r.json())

    printUsers(users)

    q('#login-form').style.display = 'none'
    q('#msg-form').style.display = 'block'
})

q('#msg-form').addEventListener('submit', async e => {
  e.preventDefault();

  let msg = q('#msg').value;

    await fetch('/send-message', {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({name, msg})
    }).then(r => r.json())

    q('#msg').value = '';

    // printMessages(msgs)
})

q('#fight').addEventListener('click', () => {
    fetch('/fight')
})

/*async function getTime(){
    let time = await fetch('/time').then(r => r.text())
    document.querySelector('div').innerHTML = time
}*/

// setInterval(getTime, 1000);